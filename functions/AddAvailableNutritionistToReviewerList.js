const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;


/**
* When a Nutritionist is available, should trigger Functions 
* to add this Professional in the last position of the Reviewer's List
* 
* OR
* 
* When a Nutritionist is unavailable, should trigger Functions 
* to remove this Professional from the Reviewer's List
*/

exports.function =
  functions.database.ref('/nutritionists/{nutritionistId}/status').onWrite(event => {

    const snapshot = event.data;

    // Only edit data when it already exists.
    if (!snapshot.previous.exists()) {
      console.log("{addAvailableNutritionistToReviewerList}: Data doesn't exists!");
      return;
    }
    // Exit when the data is deleted.
    if (!snapshot.exists()) {
      console.log("{addAvailableNutritionistToReviewerList}: Data is deleted!");
      return;
    }

    const status = snapshot.val();

    const id = event.params.nutritionistId;

    const payload = {
      id: `${id}`
    };

    if (status == "available") {
      return admin.database().ref('reviewers').push(payload);
    } else {
      return admin.database().ref("reviewers").orderByChild("id").equalTo(id)
        .once("child_added", function (childSnapshot) {
          if (childSnapshot.val().id === id) {
            return admin.database().ref('reviewers').child(childSnapshot.key).remove();
          }
        });
    }
  });