
const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;

/**
 * When a Meal is saved/created, should trigger Functions to add 
 * this Meal in the last position of the Unreviewed Meals' List.
 */

exports.function =
  functions.database.ref('/meals/{mealId}').onWrite(event => {

    const snapshot = event.data;

    // Only edit data when it is first created.
    if (snapshot.previous.exists()) {
      console.log("{addMealToUnreviewedMealsList}: Data already exists!");
      return;
    }
    // Exit when the data is deleted.
    if (!snapshot.exists()) {
      console.log("{addMealToUnreviewedMealsList}: Data is deleted!");
      return;
    }

    const id = snapshot.key;

    const payload = {
      id: `${id}`
    };
    return admin.database().ref('unreviewedMeals').push(payload);
  });
