const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;

/**
 * Add a Nutritionist professional to the Newers List
 */

exports.function =
  functions.database.ref('/nutritionists/{nutritionistId}').onWrite(event => {

    const snapshot = event.data;

    // Only edit data when it is first created.
    if (snapshot.previous.exists()) {
      console.log("{addNutritionitToNewerList}: Data already exists!");
      return;
    }
    // Exit when the data is deleted.
    if (!snapshot.exists()) {
      console.log("{addNutritionitToNewerList}: Data is deleted!");
      return;
    }

    const id = snapshot.key;

    const payload = {
      id: `${id}`
    };
    return admin.database().ref('nutritionistNewersList').push(payload);
  });