const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;

/**
 *
 * When a Nutritionist is the first of Reviewer's list,
 * should trigger Functions to notify this Professional by PushNotification.
 */

exports.function =
  functions.database.ref('/reviewers').onWrite(event => {
    const snapshot = event.data;

    snapshot.forEach((result) => {
      admin.database().ref("nutritionists").orderByKey().equalTo(result.val().id)
        .once("child_added", function (childSnapshot) {
          const fcm_token = childSnapshot.val().fcm_token;

          const payloadFCM = {
            data: {
              title: "Congrats",
              body: "Your position in the queue has gone up! Go to review a Meal right now!"
            }
          };

          return admin.messaging().sendToDevice(fcm_token, payloadFCM);
        });
      return true;
    });
  });