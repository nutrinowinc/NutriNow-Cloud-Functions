const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;

/**
 * When a Nutritionist finished a review, should trigger Functions to notify the Patient
 * by PushNotification and should put the Professional in the last position of the Reviewer's list.
 */

exports.function =
  functions.database.ref('/meals/{mealId}/review').onWrite(event => {

    const snapshot = event.data;

    // Exit when the data is deleted.
    if (!snapshot.exists()) {
      console.log("{addMealToUnreviewedMealsList}: Data is deleted!");
      return;
    }

    const mealId = event.params.mealId;
    admin.database().ref("meals").orderByKey().equalTo(mealId)
      .once("child_added", function (childSnapshot) {

        const reviewed = childSnapshot.val().reviewed;

        if (reviewed == true) {

          const professionalId = childSnapshot.val().nutritionistKey;
          const patientKey = childSnapshot.val().patientKey;

          admin.database().ref("patients").orderByKey().equalTo(patientKey)
            .once("child_added", function (childSnapshot) {
              const fcm_token = childSnapshot.val().fcm_token;

              // Get Professional and add to the last position of the Reviewer's List
              admin.database().ref("reviewers").orderByChild("id").equalTo(professionalId)
                .once("child_added", function (childSnapshot) {
                  const reviewerKey = childSnapshot.key
                  const reviewerId = childSnapshot.val().id;

                  if (reviewerId === professionalId) {

                    const payloadFCM = {
                      data: {
                        meal_id: mealId,
                        title: "Meal reviewed!",
                        body: "Your meal was reviewed, take a look!"
                      }
                    };

                    const payloadReviewer = {
                      id: professionalId
                    };

                    admin.database().ref('reviewers').child(reviewerKey).remove();
                    admin.messaging().sendToDevice(fcm_token, payloadFCM);
                    admin.database().ref('reviewers').push(payloadReviewer);
                  }
                });
            });
        }
      });
  });
