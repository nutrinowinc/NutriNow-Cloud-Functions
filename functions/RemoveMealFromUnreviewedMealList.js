const InitializeFBCF = require('./InitializeFBCF');

const functions = InitializeFBCF.functions;
const admin = InitializeFBCF.admin;


/**
* When a Meal is reviewed, should trigger Functions
* to remove this Meal from the Unreviewed's List
*/

exports.function =
  functions.database.ref('/meals/{mealId}/reviewed').onWrite(event => {

    const snapshot = event.data;

    // Only edit data when it already exists.
    if (!snapshot.previous.exists()) {
      console.log("{RemoveMealFromUnreviewedMealList}: Data doesn't exists!");
      return;
    }
    // Exit when the data is deleted.
    if (!snapshot.exists()) {
      console.log("{RemoveMealFromUnreviewedMealList}: Data is deleted!");
      return;
    }

    const reviewed = snapshot.val();

    const id = event.params.mealId;

    if (reviewed == true) {
      return admin.database().ref("unreviewedMeals").orderByChild("id").equalTo(id)
        .once("child_added", function (childSnapshot) {
          if (childSnapshot.val().id === id) {
            return admin.database().ref('unreviewedMeals').child(childSnapshot.key).remove();
          }
        });
    }
  });