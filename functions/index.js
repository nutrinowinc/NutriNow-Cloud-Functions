

const NotifyPatientWhenMealReviewed = require('./NotifyPatientWhenMealReviewed');
const AddMealToUnreviewedMeals = require('./AddMealToUnreviewedMeals');
const AddAvailableNutritionistToReviewerList = require('./AddAvailableNutritionistToReviewerList');
const NotifyNutritionistWhenFirstSpotOfReviewersList = require('./NotifyNutritionistWhenFirstSpotOfReviewersList');
const AddNutritionistToNewerList = require('./AddNutritionistToNewerList');
const RemoveMealFromUnreviewedMealList = require('./RemoveMealFromUnreviewedMealList');


exports.notifyPatientWhenMealReviewed = NotifyPatientWhenMealReviewed.function;
exports.notifyNutritionistWhenFirstSpotOfReviewersList = NotifyNutritionistWhenFirstSpotOfReviewersList.function;
exports.addMealToUnreviewedMeals = AddMealToUnreviewedMeals.function;
exports.addAvailableNutritionistToReviewerList = AddAvailableNutritionistToReviewerList.function;
exports.addNutritionistToNewerList = AddNutritionistToNewerList.function;
exports.removeMealFromUnreviewedMealList = RemoveMealFromUnreviewedMealList.function;